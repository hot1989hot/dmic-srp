<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::resource('SRP', 'SRPController');

Route::get('/login', [
    'uses' => 'PagesController@notlogin',
    'as' => 'login'
]);

Route::post('/login/custom', [
    'uses' => 'LoginController@login',
    'as' => 'login.custom'
]);

Route::post('/logout/custom', [
    'uses' => 'LoginController@logout',
    'as' => 'logout.custom'
]);

Route::group(['middleware' => 'auth'], function(){
    Route::get('/view', 'SRPController@index');
    Route::get('/submit', 'SRPController@create');
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/dashboard/{id}', 'DashboardController@show');

    Route::post('/dashboard/{id}', [
        'uses' => 'DashboardController@update',
        'as' => 'dashboard.update'
    ]);
});