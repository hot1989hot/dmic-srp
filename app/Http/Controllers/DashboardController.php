<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SRPReport;
use Auth;
class DashboardController extends Controller
{

    private $status_name;
    private $status_notion;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->status_name = array('已提交', '处理中', '已驳回', '已通过', '已支付');
        $this->status_notion = array('secondary', 'primary', 'danger', 'info', 'success');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->admin)
        {

            $reports = SRPReport::all();
            return view('dashboard')->with('reports', $reports)
            ->with('status_name', $this->status_name)
            ->with('status_notion', $this->status_notion);

        }else {
            return redirect('/')->with('error', '对不起，你并不是补给官！！');
        }

    }

    public function show($id) {
        $user = Auth::user();
        if ($user->admin) {
            $report = SRPReport::find($id);
            if($report !== null){
                return view('srpadmin')->with('report', $report)
                ->with('status_name', $this->status_name)
                ->with('status_notion', $this->status_notion);
            }else {
                return redirect('dashboard')->with('error', '该报告并不存在!');
            }
        }
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->admin) {
            $report = SRPReport::find($id);
            if($report !== null){
                $report->note = $request->note;
                $report->status = $request->status;
                $report->payout = $request->payout;
                $report->save();
                return redirect('dashboard')->with('success', '修改成功！');
            }else {
                return redirect('dashboard')->with('error', '该报告并不存在!');
            }
        }
    }


}
