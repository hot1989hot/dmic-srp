<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SRPReport;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class PagesController extends Controller
{
    public function index(){
        return view('pages.index');
    }

    public function submitSRP(){
        return view('pages.submitSRP');
    }

    public function viewSRP(){
        $reports = SRPReport::all();
        return view('pages.viewSRP')->with('reports', $reports);
    }

    public function audit(){
        return view('pages.audit');
    }

    public function notlogin(){
        return view('pages.notlogin');
    }
}