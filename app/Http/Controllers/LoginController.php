<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginController extends Controller
{
    public function login(Request $request) {
        if(
            Auth::attempt([
                'name' => $request->username,
                'password' => $request->password
            ])
        ) {
            $user = User::where('name', $request->username)->first();

            if($user->admin) {
                return redirect('/dashboard');
            }
        } else if(
            User::check_user($request->username, $request->password)
        ) {
            //Register the user
            $user = new User();
            $user->name = $request->username;
            $user->password = Hash::make($request->password);
            $user->save();
            $this->login($request);
        } else {
            return redirect('/')->with('error', '账号不存在或者密码不正确，请重试或者到 http://api.intertrick.com/ 注册！还不行请群里大声喊！');
        }
        return redirect('/view');

        return ;
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
