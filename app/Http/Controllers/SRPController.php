<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SRPReport;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Auth;
class SRPController extends Controller
{
    private $status_name;
    private $status_notion;

    public function __construct()
    {
        $this->status_name = array('已提交', '处理中', '已驳回', '已通过', '已支付');
        $this->status_notion = array('secondary', 'primary', 'danger', 'info', 'success');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        // return $user->id;
        $reports = SRPReport::where('userID', $user->id)->get();
        // $reports = SRPReport::all();
        return view('SRP.index')->with('reports', $reports)
        ->with('status_name', $this->status_name)
        ->with('status_notion', $this->status_notion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('SRP.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'link' => 'required'
        ]);
        $user = Auth::user();
        //create report
        $report = new SRPReport;
        $report->link = $request->input('link');
        $report->description = $request->input('description');
        $report->userID = $user->id;
        $report->status = 1;
        $report->note = '请求还未被处理';

        //
        $client = new Client([
            'headers' => ['content-type' => 'application/json', 'Accept' => 'application/json']
        ]);

        try{
            //get killmail details
            $res = $client->request('GET', $report->link);
        } catch (RequestException $e) {
            return redirect('/submit')->with('error', '链接不正确，请重新输入');
        }
        
        if($res->getStatusCode()!==200) {
            return redirect('/submit')->with('error', '链接不正确，请重新输入');
        }

        
        $data = $res->getBody();
        $data = json_decode($data);
        
        if (!is_object($data)) {
            return redirect('/submit')->with('error', '链接不正确，请重新输入');
        }

        
        if(DB::table('s_r_p_reports')->where('killmail_id', $data->killmail_id)->exists()){
            return redirect('/submit')->with('error', '重复的KM');
        }
        $report->killmail_id = $data->killmail_id;
        $report->killmail_time = new \DateTime($data->killmail_time);
        $report->ship_type_id = $data->victim->ship_type_id;
        $report->character_id = $data->victim->character_id;
        $report->solar_system_id = $data->solar_system_id;

        try{
            //get ship name
            $res = $client->request('GET', 'https://esi.tech.ccp.is/latest/universe/types/'.$report->ship_type_id.'/');
        } catch (RequestException $e) {
            return redirect('/submit')->with('error', '舰船ID无法找到，请联系管理员');
        }

        if($res->getStatusCode()!==200) {
            return redirect('/submit')->with('error', '舰船ID无法找到，请联系管理员');
        }

        $data = $res->getBody();
        $data = json_decode($data);

        $report->ship_name = $data->name;

        try{
            //get solarsystem name
            $res = $client->request('GET', 'https://esi.tech.ccp.is/latest/universe/systems/'.$report->solar_system_id.'/');
        } catch (RequestException $e) {
            return redirect('/submit')->with('error', '星系ID无法找到，请联系管理员');
        }

        if($res->getStatusCode()!==200) {
            return redirect('/submit')->with('error', '星系ID无法找到，请联系管理员');
        }

        $data = $res->getBody();
        $data = json_decode($data);

        $report->solar_system_name = $data->name;

        try{
            //get character name
            $res = $client->request('GET', 'https://esi.tech.ccp.is/latest/characters/'.$report->character_id);
        } catch (RequestException $e) {
            return redirect('/submit')->with('error', '人物ID无法找到，请联系管理员');
        }

        if($res->getStatusCode()!==200) {
            return redirect('/submit')->with('error', '人物ID无法找到，请联系管理员');
        }

        $data = $res->getBody();
        $data = json_decode($data);

        $report->character_name = $data->name;


        //return $report;


        $report->save();

        return redirect('/view')->with('success', '战损已提交');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
