<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function check_user($username, $password) {
        $client = new Client([
            'headers' => [
                'Accept' => 'application/json',
                'X-Token' => 'oP52btoAq0y2lQcFIdVeJPf4DWgsF1Vx'
                ]
        ]);

        try{
            //Check username and password
            $res = $client->request(
                'POST', 
                'http://api.intertrick.com/api/v1/user/auth/login/',
                [
                    'form_params' => [
                        'username' => $username,
                        'password' => $password
                    ]
                ]
            );
        } catch (RequestException $e) {
            return false;
            // return redirect('/')->with('error', 'ERROR:RequestException 账户不存在或密码错误！！');
        }        
        if($res->getStatusCode()!==200) {
            return false;
            // return redirect('/')->with('error', 'ERROR:200 - 账户不存在或密码错误！！');
        }

        $data = $res->getBody();
        $data = json_decode($data);
        return $data;
        
    }
}
