<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSRPReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_r_p_reports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('link');
            $table->integer('killmail_id')->unique();
            $table->dateTimeTz('killmail_time');
            $table->integer('ship_type_id');
            $table->string('ship_name');
            $table->integer('userID');
            $table->integer('character_id');
            $table->string('character_name');
            $table->text('description')->nullable();
            $table->integer('solar_system_id');
            $table->string('solar_system_name');

            $table->bigInteger('payout')->nullable();
            $table->integer('adminID')->nullable();
            $table->integer('status');
            $table->text('note')->nullable();
            $table->dateTime('processed_at')->nullable();

            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_r_p_reports');
    }
}
