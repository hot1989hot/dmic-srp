

<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">首页</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/submit">
                        提交战损
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/view">查看战损</a>
                </li>
                @if(Auth::user() && Auth::user()->admin)
                <li class="nav-item">
                    <a class="nav-link" href="/dashboard">审核</a>
                </li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <form action="{{route('login.custom')}}" method="POST">
                {{ csrf_field() }}
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li><input class="form-control" type="text" name="username" id="username"></li>
                        <li><input class="form-control" type="password" name="password" id="password"></li>
                        <li><button class="form-control" type="submit">登陆</button></li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout.custom') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                                </a>
                            </div>
                        </li>
                    @endguest
                </ul>
            </form>
            <form id="logout-form" action="{{ route('logout.custom') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</nav>