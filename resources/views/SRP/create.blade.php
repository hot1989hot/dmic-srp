@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>SRP Create Report</h1>

        <form action="{{action('SRPController@store')}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="link" class="col-sm-1 col-form-label">KB链接</label>
                <div class="col-sm-11">
                    <input type="text" class="form-control" id="link" name="link">
                </div>
            </div>
    
            <div class="form-group row">
                <label for="description" class="col-sm-1 col-form-label">说明</label>
                <div class="col-sm-11">
                    <textarea class="form-control" name="description" id="description" cols="30" rows="10" maxlength="200"></textarea>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-10 offset-sm-1">
                    <button type="submit" class="btn btn-primary">提交</button>
                    <button class="btn btn-danger">清空</button>
                </div>
            </div>
        </form>
    
    </div>
@endsection