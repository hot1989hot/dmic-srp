@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>SRP Reports</h1>
        <div class="row">
        @if( count($reports) > 0)
            @foreach($reports as $report)
                <div class="col-md-4">
                    <div class="card flex-md-row mb-4 box-shadow h-md-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <h2 class="w-100">
                                <span class="badge badge-{{$status_notion[$report->status]}} ">{{$status_name[$report->status]}}</span>
                                @if($report->status===4)
                                    <small class="text-muted blockquote float-right" style="margin-top: 10px;">{{$report->payout}} ISK</small>
                                @endif
                            </h2>
                            <h4 class="mb-0">
                            {{$report->character_name}} 的 {{$report->ship_name}}
                            </h4>
                            <a href="https://zkillboard.com/kill/{{$report->killmail_id}}/" target="_blank">
                                <i class="fas fa-external-link-alt"></i>zkb
                            </a>
                            <div class="mb-1 text-muted">时间：{{$report->killmail_time}}</div>
                            <div class="mb-1 text-muted">地点：{{$report->solar_system_name}}</div>
                            <div class="mb-1 text-muted disabled">消息：{{$report->note}}</div>
                            <textarea cols="40" rows="5" style="resize:none" disabled>{{$report->description}}</textarea>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <p>No Report Found!</p>

        @endif
        </div>
    </div>
@endsection