@extends('layouts.app')

@section('content')

<div class="row">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
</div>
<h1>补给官快发钱啊！！</h1>
<div class="row">
    <div class="col-md-4">
        <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body">
                <h2><span class="badge badge-{{$status_notion[$report->status]}}">{{$status_name[$report->status]}}</span></h2>
                <h4 class="mb-0">
                {{$report->character_name}} 的 {{$report->ship_name}}
                </h4>
                <a href="https://zkillboard.com/kill/{{$report->killmail_id}}/" target="_blank">
                    <i class="fas fa-external-link-alt"></i>zkb
                </a>
                <div class="mb-1 text-muted">时间：{{$report->killmail_time}}</div>
                <div class="mb-1 text-muted">地点：{{$report->solar_system_name}}</div>
                <div class="mb-1 text-muted disabled">消息：{{$report->note}}</div>
                <textarea cols="40" rows="5" style="resize:none" disabled>{{$report->description}}</textarea>
                <form action="{{ action('DashboardController@update', [$report->id])}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="note">补给官备注/留言：</label>
                    <textarea class="form-control" name="note" id="note" cols="40" rows="5" >{{$report->note}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="status">处理状态：</label>
                        <select class="form-control" id="status" name="status" size=5>
                            <option value=0 {{$report->status==0?'selected':''}}>已提交</option>
                            <option value=1 {{$report->status==1?'selected':''}}>处理中</option>
                            <option value=2 {{$report->status==2?'selected':''}}>已驳回</option>
                            <option value=3 {{$report->status==3?'selected':''}}>已通过</option>
                            <option value=4 {{$report->status==4?'selected':''}}>已支付</option>
                        </select>
                    </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">补给金额</div>
                            </div>
                        <input type="number" class="form-control" name="payout" id="payout" value="{{$report->payout}}">
                            <div class="input-group-postpend">
                                <div class="input-group-text">ISK</div>
                            </div>
                        </div>
                    <button class="btn btn-success" type="submit">提交</button>
                </form>
            </div>
            
        </div>
    </div>
    <iframe id="kmframe" class="col-md-8" src="https://zkillboard.com/kill/{{$report->killmail_id}}/" frameborder="0" sandbox></iframe>
</div>

@endsection
