@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <h1>补给官快发钱啊！！</h1>
    <div class="row">
        @if( count($reports) > 0)
            @foreach($reports as $report)
                <div class="col-md-4">
                    <div class="card flex-md-row mb-4 box-shadow h-md-250">
                        <div class="card-body">
                            <h2 class="w-100">
                                <span class="badge badge-{{$status_notion[$report->status]}} ">{{$status_name[$report->status]}}</span>
                                @if($report->status===4)
                                    <small class="text-muted blockquote float-right" style="margin-top: 10px;">{{$report->payout}} ISK</small>
                                @endif
                            </h2>
                            <h4 class="mb-0">
                            {{$report->character_name}} 的 {{$report->ship_name}}
                            </h4>
                            <div class="mb-1 text-muted">时间：{{$report->killmail_time}}</div>
                            <div class="mb-1 text-muted">地点：{{$report->solar_system_name}}</div>
                            <div class="mb-1 text-muted disabled">消息：{{$report->note}}</div>
                            <textarea cols="40" rows="5" style="resize:none" disabled>{{$report->description}}</textarea>
                            {{-- <form>
                                <div class="form-group">
                                    <label for="note">补给官备注/留言：</label>
                                    <textarea name="note" id="note" cols="40" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="status">处理状态：</label>
                                    <select class="form-control" id="status" name="status" size=5>
                                        <option value=0 {{$report->status==0?'selected':''}}>已提交</option>
                                        <option value=1 {{$report->status==1?'selected':''}}>处理中</option>
                                        <option value=2 {{$report->status==2?'selected':''}}>已驳回</option>
                                        <option value=3 {{$report->status==3?'selected':''}}>已通过</option>
                                        <option value=4 {{$report->status==4?'selected':''}}>已支付</option>
                                    </select>
                                </div>
                                <button class="btn btn-success" type="submit">更改</button>
                            </form> --}}
                        <a href="/dashboard/{{$report->id}}">打开</a>
                        </div>
                        
                    </div>
                </div>
            @endforeach
        @else
            <p>No Report Found!</p>
        @endif
    </div>
</div>

@endsection